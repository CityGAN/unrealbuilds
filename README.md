# UnrealBuilds

This repository holds the links to the pakaged newest Versions of the Linux and Windows build of the visualization project

## Download the most recent builds here
Windows: https://fh-swf.sciebo.de/s/pE2JHo6UZ6w9rBd 

## Getting started
0. Check for graphic driver updates to your system
1. Download the most recent build for your system
2. Start the packaged project
    - For Windows start the CitySpawn.exe file 
    - For Linux navigate to the download folder and execute the CitySpawn.sh file (make sur you have the correct access rights) 
3. Choose if you want to join or host a multiplayer session
    - For hosting enter a username to start up the server. If you want to be a remote host make sure your IP is visible and port 7777 is open.
    - for connecting to a remote host select: join session and enter the IP adress

## License
This Project is Licensed under the MIT License

